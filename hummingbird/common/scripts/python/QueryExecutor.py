#!/usr/bin/env /usr/bin/python

import os, sys
import subprocess
# sys.path.insert(0, os.getcwd())
import HBConstants
from HBConfigUtility import JsonConfigUtility
from HBCommonUtility import HBCommonUtility

PROCESS_NAME = HBConstants.QUERY_EXECUTOR
process_step = ""


class QueryExecutor(object):
    def __init__(self):
        self.query_params = None
        self.stage_params = None
        self.env_params = None
        self.constants_conf = None
        self.hb_common_utils = None

        os.environ[HBConstants.PYTHON_EGG_CACHE_KEY] = HBConstants.PYTHON_EGG_CACHE_PATH_KEY
        
    def executor_handler(self, subject_area=None, stage=None):
        try:
			global process_step
			# TODO: logger - process started

			if subject_area is None or stage is None:
				raise Exception("Invalid arguments passed")

			process_step = HBConstants.PROCESS_STEP.__add__('_1')
			try:
				self.hb_common_utils = HBCommonUtility()
				self.query_params = JsonConfigUtility(conf_file=HBConstants.QUERY_PARAMETERS_FILE).get_config()
				self.env_params = JsonConfigUtility(conf_file=HBConstants.QUERY_ENVIRONMENT_FILE).get_config()
				# command = "echo Adhoc.12345 | kinit kcvz834@AMERICAS.ASTRAZENECA.NET"
				# self.hb_common_utils.execute_shell_command(command)
			except Exception as e:
				raise Exception(str(e))

			self.stage_params = self.fetch_param_config(subject_area, stage)
			if self.stage_params is None:
				# ToDo: Exception MESSAGE
				raise Exception("Invalid arguments passed. subject area - "+ subject_area + ", stage - "+ stage)

			process_step = HBConstants.PROCESS_STEP.__add__('_2')
			query_file = self.get_target_file_path()
			if not self.validate_target_file(query_file):
				# ToDo: Exception MESSAGE
				raise Exception("File doesn't exists: " + query_file)

			process_step = HBConstants.PROCESS_STEP.__add__('_3')
			local_path = os.getcwd()
			if not self.copy_file_to_local(query_file, local_path):
				# ToDo: Exception MESSAGE
				raise Exception("Error while copying file "+ query_file +" to location " + local_path)

			process_step = HBConstants.PROCESS_STEP.__add__('_4')
			query_type = self.stage_params.get(HBConstants.QUERY_TYPE_KEY)
			local_file = local_path + "/" + self.stage_params.get(HBConstants.QUERY_TARGET_FILE_NAME_KEY)
                        try:
                                self.execute_query(query_type, local_file)
                                cmd = HBConstants.LINUX_REMOVE_COMMAND + " " + local_file
				if not self.hb_common_utils.execute_shell_command(cmd):
					raise Exception(str(e))

				# ToDo : looger
			except Exception as e:
				cmd = HBConstants.LINUX_REMOVE_COMMAND + " " + local_file
				if not self.hb_common_utils.execute_shell_command(cmd):
					raise Exception(str(e))

				# ToDo : looger
				raise Exception(str(e))
        except Exception as e:
            status_msg = process_step + " of " + PROCESS_NAME + "terminated. \r\n\nERROR MESSAGE: " + str(e)
            raise Exception
            # ToDo: logger
            
    def execute_query(self, query_type=None, query_file=None):
		try:
			if query_file is None or query_type is None:
				# ToDo: MESSAGE
				raise Exception("execute_query() - Invalid arguments")


			if query_type == HBConstants.HIVE_KEY:
				hive_host = self.env_params.get(HBConstants.HIVE_HOST_KEY)
				hive_port = self.env_params.get(HBConstants.HIVE_PORT_KEY)
				kerberos_realm = self.env_params.get(HBConstants.KERBEROS_REALM_KEY)

				cmd = 'beeline --verbose=true -u  "jdbc:hive2://'+ hive_host +':'+ str(hive_port) +'/default;principal=hive/_HOST@'+ kerberos_realm +'" -f ' + query_file
				# cmd = 'beeline --verbose=true -u  "jdbc:hive2://'+ hive_host +':'+ str(hive_port) +'/default;principal=hive/_HOST@'+ kerberos_realm +'" -e "use hb_publish; show tables;"'
			elif query_type == HBConstants.IMPALA_KEY:
				impalad_hostname = self.env_params.get(HBConstants.IMPALAD_HOSTNAME_KEY)
				cmd = 'impala-shell -k --ssl -i '+ impalad_hostname + ' -f ' + query_file
			else:
				raise Exception("Invalid query type  passed: " + query_type)

			self.hb_common_utils.execute_shell_command(cmd)

		except Exception as e:
			raise Exception(str(e))


    def get_target_file_path(self):
        return self.stage_params.get(HBConstants.QUERY_TARGET_FILE_LOCATION_KEY) + "/" + self.stage_params.get(HBConstants.QUERY_TARGET_FILE_NAME_KEY)


    def validate_target_file(self, file_path):
        cmd = "hdfs dfs -test -e " + file_path
        return self.hb_common_utils.execute_shell_command(cmd)


    def copy_file_to_local(self, file_path, local_path):
        cmd = "hdfs dfs -get " + file_path + " " + local_path
        return self.hb_common_utils.execute_shell_command(cmd)

    def fetch_param_config(self, subject_area=None, stage=None):
        for param in self.query_params:
            if param is not None:
                if param.get(HBConstants.SUBJECT_AREA_KEY) == subject_area and param.get(HBConstants.STAGE_KEY) == stage:
                    return param
                else:
					continue
        return None

#q = QueryExecutor()
#q.executor_handler("Bladder", "LOT")