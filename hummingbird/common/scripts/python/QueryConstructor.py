#!/usr/bin/env /usr/bin/python
import os, sys

sys.path.insert(0, os.getcwd())
from HBConfigUtility import JsonConfigUtility
from HBCommonUtility import HBCommonUtility
import HBConstants

PROCESS_NAME = HBConstants.QUERY_CONSTRUCTOR


class QueryConstructor(object):
    process_name = HBConstants.QUERY_CONSTRUCTOR

    def __init__(self):
        process_step = ''
        try:
            # log audit for process start
            process_step = HBConstants.PROCESS_STEP.__add__('_1')
            self.query_params = JsonConfigUtility(conf_file=HBConstants.QUERY_PARAMETERS_FILE).get_config()
            self.query_templates = JsonConfigUtility(conf_file=HBConstants.QUERY_TEMPLATES_FILE).get_config()
            
        except Exception as e:
            status_msg = process_step + " of " + PROCESS_NAME+ " terminated. Loading Config files failed. " + str(e)
            # log audit for failure
            raise Exception(status_msg)
        self.hm_common_utils = HBCommonUtility()

    def construct_query(self, subject_area=None, stage=None):
        process_step = ''
        status_msg = ''
        seq_queryid_dict = dict()
        query_confs = []
        try:
            process_step = HBConstants.PROCESS_STEP.__add__('_2')
            param = self.fetch_param_config(subject_area, stage)
            if param is None:
                status_msg = "No Matching config found in query parameters config file. "
                raise Exception(status_msg)
            configs = param.get(HBConstants.CONFIGS_KEY)
            file_name = param.get(HBConstants.QUERY_TARGET_FILE_NAME_KEY)
            location = param.get(HBConstants.QUERY_TARGET_FILE_LOCATION_KEY)

            if configs is not None:
                for config in configs:
                    if str(config.get(HBConstants.ENABLED_FLAG_KEY)).strip().__eq__("Y"):
                        seq_queryid_dict[config.get(HBConstants.QUERY_SEQUENCE_KEY)] = config
            else:
                status_msg = "No query configs found in the matching query template "
                raise Exception(status_msg)
            if seq_queryid_dict:
                for seq in sorted(seq_queryid_dict.iterkeys()):
                    query_confs.append(seq_queryid_dict[seq])
            # log audit for fetch of query configs
            process_step = HBConstants.PROCESS_STEP.__add__('_3')
            queries = self.fetch_queries_from_templates(query_confs)

            # log audit for fetch of queries from templates

            process_step = HBConstants.PROCESS_STEP.__add__('_4')
            if queries:
                hdfs_location_exists = self.hm_common_utils.check_hdfs_directory_exists(location)
                if hdfs_location_exists:
                    self.write_to_target_location(queries, file_name, location)
                else:
                    status_msg = "Target hdfs location " + location + " doesn't exist "
                    raise Exception(status_msg)

                    # log audit for success write to target file

                    # log audit for completion of process

        except Exception as e:
            status_msg = process_step + " of " + PROCESS_NAME + " terminated. \r\n\nERROR MESSAGE: " + str(e)
            # logging audit for all failures
            raise Exception(status_msg)

    def write_to_target_location(self, queries, file_name, hdfs_location):
        try:
            self.hm_common_utils.cleanup_hdfs_file(hdfs_location + '/' + file_name)
            query_file = open(file_name, "w+")
            for query in queries:
                query_file.write(str(query).strip().__add__("\r\n\n"))
            query_file.close()
            command = str(HBConstants.HDFS_PUT_COMMAND).__add__(' ' + file_name + ' ').__add__(hdfs_location)
            file_write_success = self.hm_common_utils.execute_shell_command(command)
            if not file_write_success:
                status_msg = "Query write to HDFS file failed"
                raise Exception(status_msg)
            else:
                command = str(HBConstants.HDFS_CHMOD_COMMAND + ' ' + HBConstants.QUERY_FILE_PERMISSION + ' ').__add__(
                    hdfs_location).__add__('/' + file_name)
                permission_set = self.hm_common_utils.execute_shell_command(command)
            if permission_set:
                command = str(HBConstants.LINUX_REMOVE_COMMAND + ' ').__add__(file_name)
                self.hm_common_utils.execute_shell_command(command)
            else:
                raise Exception("Setting permission of HDFS file(s) failed")
        except Exception as e:
            raise e

    def fetch_param_config(self, subject_area=None, stage=None):
        for param in self.query_params:
            if param is not None:
                if param.get(HBConstants.SUBJECT_AREA_KEY) == subject_area and param.get(
                        HBConstants.STAGE_KEY) == stage:
                    return param
                else:
                    continue
        return None

    def fetch_queries_from_templates(self, query_confs):
        queries = []
        if not query_confs:
            status_msg = "No configs fetched from query parameters config file"
            raise Exception(status_msg)
        if self.query_templates:
            for conf in query_confs:
                fetched_query = None
                for query_template in self.query_templates:
                    if query_template.get(HBConstants.QUERY_ID_KEY) == conf.get(HBConstants.QUERY_ID_KEY):
                        query = query_template.get(HBConstants.QUERY_TEMPLATE_KEY)
                        if query is not None and query:
                            # replace variables in hql
                            query_replaced_variable = self.hm_common_utils.replace_variables(query,
                                                                                             conf.get('variables'))
                            fetched_query = query
                            queries.append(query_replaced_variable)
                        else:
                            status_msg = "Query Template is empty for query id  " + str(conf.get(HBConstants.QUERY_ID_KEY))
                            raise Exception(status_msg)
                if fetched_query is None:
                    status_msg = "Query Id " + str(conf.get(HBConstants.QUERY_ID_KEY)) + " not found in query templates config file." 
                    raise Exception(status_msg)               
        else:
            status_msg = "Query templates are empty "
            raise Exception(status_msg)
        return queries


if __name__ == "__main__":
    subj_area = None
    data_src_name = None
    app_env_file_path = None
    ld_step = None
    custom_var_path = None
    begin_step = None

    # Instantiate and call the Process Manager class
    query_constructor = QueryConstructor()
    query_constructor.construct_query("Bladder", "LOT1")
