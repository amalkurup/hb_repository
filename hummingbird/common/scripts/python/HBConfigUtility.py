#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
MODULE_NAME = "JSONConfigUtility"

# ################################ Class JsonConfigUtility #############################################################
# Class contains all the functions related to JsonConfigUtility
# ######################################################################################################################


class JsonConfigUtility(object):

    # Parametrized constructor with Configuration file as input
    def __init__(self, conf_file=None):
        print "conf file==>>"+str(conf_file)
        try:
            if conf_file is not None:
                config_fp = open(conf_file)
                print "config file fouund"
                self.json_configuration = json.load(config_fp)
                print "config==>>"+str(self.json_configuration)
            else:
                self.json_configuration = {}
        except Exception as e:
            raise Exception("Error Loading "+conf_file+ ". " + str(e))

    # ############################################### Get Configuration ################################################
    # Purpose            :   This method will read the value of a configuration parameter corresponding to
    #                        a section in the configuration file
    #
    # Input              :   Section in the configuration file, Configuration Name
    #
    # Output             :   Returns the value of the configuration parameter present in the configuration file
    # ##################################################################################################################
    def get_configuration(self, conf_hierarchy):
        try:
            return reduce(lambda dictionary, key: dictionary[key], conf_hierarchy, self.json_configuration)
        except:
            raise Exception

    def get_config(self):
            try:
                print "get_config called"
                return self.json_configuration
            except Exception as e:
                print e
                raise Exception(str(e))

