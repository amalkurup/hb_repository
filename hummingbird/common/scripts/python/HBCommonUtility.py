import subprocess
import logging
from HBConfigUtility import JsonConfigUtility
import HBConstants


class HBCommonUtility(object):
    def __init__(self, execution_context=None):
        pass

    def replace_variables(self, raw_string, dict_replace_params):
        status_message = ""
        try:
            # Sort the dictionary keys in ascending order
            dict_keys = dict_replace_params.keys()
            dict_keys.sort(reverse=True)
            # Iterate through the dictionary containing parameters for replacements and replace all the variables in the
            # raw string with their values
            for key in dict_keys:
                raw_string = raw_string.replace(str(HBConstants.VARIABLE_PREFIX + key + HBConstants.VARIABLE_PREFIX),
                                                dict_replace_params[key])
            if str(raw_string).__contains__(HBConstants.VARIABLE_PREFIX):
                status_message = "Variable value(s) not found in query parameter config files"
                raise Exception(status_message)

            return raw_string
        except Exception as e:
            raise e

    def execute_shell_command(self, command):
        method_name = ""
        try:
            command_output = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            standard_output, standard_error = command_output.communicate()
            print standard_error 
            if command_output.returncode == 0:
                return True
            elif command_output.returncode == 1:
                return False
            elif command_output.returncode > 1:
                status_message = "Error occurred while executing command on shell :" + command
                raise Exception(status_message)
        except Exception as e:
            raise e

    def refresh_kerberos(self):
        try:
            environment_params = JsonConfigUtility(conf_file="hb_environment_parameters.json")
            keytab_file = environment_params.get_configuration(["kerberos_keytab_file"])
            principal = environment_params.get_configuration(["kerberos_principal"])
            if keytab_file is not None and principal is not None:
                kerberos_cache_name = "`klist | grep 'Ticket cache:' | cut -d':' -f3`"
                command = str('kinit ').__add__(' -k -t ').__add__(keytab_file).__add__(
                    ' ' + principal)
                self.execute_shell_command(command)
                self.execute_shell_command('klist')
                
            else:
                status_msg = "Keytab file or Principal name is not found in the configuration file "
                raise Exception(status_msg)
        except Exception as e:
            # Not valid
            raise e

    def check_hdfs_file_exist(self, file_path):
        try:
            command = str(HBConstants.HDFS_TEST_FILE_EXISTS_COMMAND + ' ' + file_path)
            return self.execute_shell_command(command)
        except Exception as e:
            raise e

    def check_hdfs_directory_exists(self, location):
        try:
            command = str(HBConstants.HDFS_TEST_FOLDER_EXISTS_COMMAND + ' ' + location)
            return self.execute_shell_command(command)
        except Exception as e:
            raise e

    def cleanup_hdfs_file(self, file_path):
        try:
            file_exists = self.check_hdfs_file_exist(file_path)
            if file_exists:
                command = str(HBConstants.HDFS_REMOVE_FILE_COMMAND + ' ' + file_path)
                return self.execute_shell_command(command)
        except Exception as e:
            raise e
