PROCESS_STEP = "p_step"

CONFIGS_KEY = "configs"
QUERY_TARGET_FILE_NAME_KEY = "target_file_name"
QUERY_TARGET_FILE_LOCATION_KEY = "target_location"
QUERY_ID_KEY = "query_id"
QUERY_SEQUENCE_KEY = "sequence"
DATA_SOURCE_KEY = "data_source"
SUBJECT_AREA_KEY = "subject_area"
STAGE_KEY = "stage"
QUERY_TEMPLATE_KEY = "query_template"
QUERY_VARIABLES_KEY = "variables"
ENABLED_FLAG_KEY = "enable_flag"
QUERY_EXECUTOR="Query Executor Module"

QUERY_CONSTRUCTOR = "Query Constructor Module"
VARIABLE_PREFIX = "$$"

DATA_TRANSFORMATION_WRAPER = "DataTransformationWrapper"

QUERY_PARAMETERS_FILE = "hb_query_parameters.json"
QUERY_TEMPLATES_FILE = "hb_query_templates.json"
QUERY_ENVIRONMENT_FILE = "hb_environment_parameters.json"

QUERY_FILE_PERMISSION = "777"

HDFS_PUT_COMMAND = "hdfs dfs -put"
HDFS_CHMOD_COMMAND = "hdfs dfs -chmod"
HDFS_TEST_FILE_EXISTS_COMMAND = "hdfs dfs -test -e"
HDFS_TEST_FOLDER_EXISTS_COMMAND = "hdfs dfs -test -d"
HDFS_REMOVE_FILE_COMMAND = "hdfs dfs -rm"

LINUX_REMOVE_COMMAND = "rm"
QUERY_TYPE_KEY = "query_type"
ENVIRONMENT_KEY = "environment"
CURRENT_USER_KEY = "current_user"
SEC_USER_1 = "sec_user_1"
SEC_USER_2 = "sec_user_2"
KERBEROS_PRINCIPAL_KEY = "kerberos_principal"
KERBEROS_PASSWORD_KEY = "kerberos_password"
KERBEROS_KEYTAB_FILE_KEY = "kerberos_keytab_file"
KERBEROS_KEYTAB_LOCATION_KEY = "kerberos_keytab_location"
KERBEROS_REALM_KEY = "kerberos_realm"
IMPALAD_HOSTNAME_KEY = "impalad_hostname"
HIVE_HOST_KEY = "hive_host"
HIVE_PORT_KEY = "hive_port"
COMMON_AWS_ACCESS_KEY = "common_aws_access_key_id"
COMMON_AWS_SECRET_ACCESS_KEY = "common_aws_secret_access_key"
COMMON_AWS_DEFAULT_REGION_KEY = "common_aws_default_region"
COMMON_S3_BUCKET_KEY = "common_s3_bucket"
COMMON_FTP_HOST_KEY = "common_ftp_host"
COMMON_FTP_PORT_KEY = "common_ftp_port"

PYTHON_EGG_CACHE_KEY = "PYTHON_EGG_CACHE"
PYTHON_EGG_CACHE_PATH_KEY = "/tmp/python-egg"
HIVE_KEY = "hive"
IMPALA_KEY = "impala"
