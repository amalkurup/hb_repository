#!/usr/bin/env /usr/bin/python
import os
import sys

sys.path.insert(0, os.getcwd())
# from LogSetup import logger
from QueryConstructor import QueryConstructor
from QueryExecutor import QueryExecutor
from HBConfigUtility import JsonConfigUtility
from HBCommonUtility import HBCommonUtility
import HBConstants

PROCESS_NAME = HBConstants.DATA_TRANSFORMATION_WRAPER

class DataTransformationWrapper(object):
    def __init__(self, subject_area, stage):
        # Call audit function with a message "Process Name Started"        
        self.subject_area = subject_area
        self.stage = stage

    def transform_data(self):
        process_step = ""
        try:
            
            process_step = HBConstants.PROCESS_STEP.__add__('_2')
            # Call audit function with a message "Calling Query Constructor Module"
            HQLConstructor_OBJ = QueryConstructor()
            print str(HQLConstructor_OBJ )
            flag_construct = HQLConstructor_OBJ.construct_query(self.subject_area,self.stage)
            if True:
                process_step = HBConstants.PROCESS_STEP.__add__('_3')
                # Call audit function with a message "Calling Query Executor Module"
                HQLExecutor_OBJ = QueryExecutor()
                flag_executor = HQLExecutor_OBJ.executor_handler(self.subject_area,self.stage)
                    
            else:
                # Call audit function with a message "Query Constructor Module Failed"
                status_msg = "terminated. Contructor failed"
                raise Exception(status_msg)
        except Exception as e:
            status_msg = process_step + " of " + PROCESS_NAME + " terminated. \r\n\nERROR MESSAGE: " + str(e)
            # logging audit for all failures
            raise Exception(status_msg)


if __name__ == "__main__":
    process_step = HBConstants.PROCESS_STEP.__add__('_1')
    try:
        #subject_area = "LUNG"
        #stage = "Staging"
        subject_area = str(os.environ[HBConstants.SUBJECT_AREA_KEY]).strip()
        stage = str(os.environ[HBConstants.STAGE_KEY]).strip()
        print "INPUTS -->>"+subject_area+stage
        if not subject_area or not stage:
            # Call audit function with a message "Parameters are not Valid"
            status_msg = " terminated. Parameters are not valid"
            raise Exception(status_msg)
            
        else:
            hmbird_commmon_utility_obj = HBCommonUtility()
            hmbird_commmon_utility_obj.refresh_kerberos()
            transformation_wrapper_obj = DataTransformationWrapper(subject_area, stage)
            transformation_wrapper_obj.transform_data()

    except Exception as e:
            status_msg = process_step + " of " + PROCESS_NAME + "terminated. \r\n\nERROR MESSAGE: " + str(e)
            # logging audit for all failures
            raise Exception(status_msg)